## 1.0.0

##### Breaking

* None.

##### Enhancements

* Update Bazel config to allow targets to be directly consumed.  
  [Maxwell Elliott](https://github.com/maxwellE)